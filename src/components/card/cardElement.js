// import template from './card-element.html'
// const template = document.getElementById('card-element')

export default (Card, containerEl) => {
  // const _element = template.content.cloneNode(true)
  const _element = new Card()

  return {
    get element () => {
      return _element
    }
    destroy () => {
      // clean up any event listeners?
    }
  }
}
