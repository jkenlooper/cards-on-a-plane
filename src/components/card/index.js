import TestDivElement from './testDivElement.js'
import {getCardList} from '../../store'

export default class CardContainer {
  constructor (store, element) {
    this.store = store
    this.testDivElement = new TestDivElement(element)
  }

  update () {
    console.log(getCardList(this.store))
    this.testDivElement.addCards(getCardList(this.store))
  }

  init () {
    this.store.subscribe(this.update.bind(this))
  }
}
