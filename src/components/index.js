import PileComponent from './pile.js'
import TableComponent from './table.js'

export {
  TableComponent,
  PileComponent
}
