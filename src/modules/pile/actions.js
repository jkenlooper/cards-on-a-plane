import {CARDLIST_REPLACE, CARD_ADD} from '../card-list/constants.js'

// Actions
// Follow FSA https://github.com/acdlite/flux-standard-action

export function moveCard (props) {
  return {
    type: CARD_MOVE,
    payload: props
  }
}

