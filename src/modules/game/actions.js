import {GAME_START} from './constants.js'

export function startGame () {
  return {
    type: GAME_START
  }
}
