import { loadState, saveState } from './localStorage'
import withinBounds from './withinBounds.js'

export {
  withinBounds,
	loadState,
	saveState
}
