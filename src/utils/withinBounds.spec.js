const tap = require('tap')
const withinBounds = require('./withinBounds.tmp.js')

tap.test('withinBounds', (t) => {
  const box1 = {
    x: '23',
    y: '12',
    width: '100',
    height: '200'
  }
  const box2 = {
    x: '2',
    y: '1',
    width: '200',
    height: '300'
  }

  t.equal(withinBounds(box1, box2), true, 'is within bounds')

  box1.x = '205'
  t.equal(withinBounds(box1, box2), false, 'is not within bounds')

  box1.x = '200'
  box1.y = '300'
  t.equal(withinBounds(box1, box2), true, 'is within bounds')

  box1.x = '200'
  box1.y = '330'
  t.equal(withinBounds(box1, box2), false, 'is not within bounds')

  t.end()
})
