/* globals Redux */

import {loadState} from '../utils'
import reducers from '../modules/reducers.js'

export default function configureStore (persistedState = loadState()) {
  return Redux.createStore(reducers, persistedState)
}
